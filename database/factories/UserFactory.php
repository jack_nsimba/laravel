<?php

use Faker\Generator as Faker;


$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', // sha1 secret
        'remember_token' => str_random(10),
    ];
});
